<?php

namespace Jumbojett;

use Exception;

trait UrlEncoderTrait
{
    /**
     * Per RFC4648, "base64 encoding with URL-safe and filename-safe
     * alphabet".  This just replaces characters 62 and 63.  None of the
     * reference implementations seem to restore the padding if necessary,
     * but we'll do it anyway.
     *
     * @param string $b64_url
     *
     * @return string
     */
    private static function b64url2b64(string $b64_url): string
    {
        // "Shouldn't" be necessary, but why not
        $padding = strlen($b64_url) % 4;
        if ($padding > 0) {
            $b64_url .= str_repeat('=', 4 - $padding);
        }

        return strtr($b64_url, '-_', '+/');
    }

    /**
     * A wrapper around base64_decode which decodes Base64URL-encoded data,
     * which is not the same alphabet as base64.
     *
     * @param string $b64_url
     *
     * @return string
     * @throws Exception
     */
    private static function base64url_decode(string $b64_url): string
    {
        $url = base64_decode(self::b64url2b64($b64_url));

        if ($url === false) {
            throw new Exception();
        }

        return $url;
    }
}