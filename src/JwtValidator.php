<?php

namespace Jumbojett;

class JwtValidator
{
    use UrlEncoderTrait;

    /**
     * @param Jwt      $jwt encoded JWT
     * @param callable $jwks_accessor
     * @param string   $client_secret
     *
     * @return bool
     * @throws OpenIDConnectClientException
     */
    public function verifyJwtSignature(Jwt $jwt, callable $jwks_accessor, string $client_secret)
    {
        $header    = $jwt->getDecodedHeader();
        $signature = $jwt->getDecodedSignature();

        $payload = $jwt->getHeaderPayload();

        // Todo: Checks

        $alg = $jwt->getHeaderAlg();

        switch ($alg) {
            case 'RS256':
            case 'PS256':
            case 'RS384':
            case 'RS512':
                $jwks = call_user_func($jwks_accessor);

                if ($jwks === null) {
                    throw new OpenIDConnectClientException('Error decoding JSON from jwks_uri');
                }

                $hashtype      = 'sha' . substr($alg, 2);
                $signatureType = ($alg === 'PS256') ? 'PSS' : '';

                $verified = $this->verifyRSAJWTSignature(
                    $hashtype,
                    $this->getKeyForHeader($jwks['keys'], $header),
                    $payload,
                    $signature,
                    $signatureType
                );
                break;

            case 'HS256':
            case 'HS512':
            case 'HS384':
                // No need of jwks_uri here
                $hashtype = 'SHA' . substr($alg, 2);
                $verified = $this->verifyHMACJWTSignature($hashtype, $client_secret, $payload, $signature);
                break;

            default:
                throw new OpenIDConnectClientException('No support for signature type: ' . $alg);
        }

        return $verified;
    }

    /**
     * @param array       $claims
     * @param Jwt         $id_token
     * @param string|null $access_token
     * @param callable    $claims_validator
     *
     * @return bool
     */
    public function verifyJwtClaims(array $claims, Jwt $id_token, ?string $access_token, callable $claims_validator)
    {
        $expected_at_hash = null;

        if (isset($claims['at_hash']) && isset($access_token)) {
            try {
                $alg = $id_token->getHeaderAlg();

                if ($alg === 'none') {
                    throw new \Exception();
                }

                $bit = substr($alg, 2, 3);
            } catch (\Exception $e) {
                // TODO: Error case. throw exception???
                $bit = '256';
            }

            $len              = ((int)$bit) / 16;
            $expected_at_hash = $this->urlEncode(substr(hash('sha' . $bit, $access_token, true), 0, $len));
        }

        return call_user_func($claims_validator, $claims, $access_token, $expected_at_hash);
    }

    /**
     * @param string $str
     *
     * @return string
     */
    protected function urlEncode($str)
    {
        $enc = base64_encode($str);
        $enc = rtrim($enc, '=');
        $enc = strtr($enc, '+/', '-_');

        return $enc;
    }

    /**
     * @param array $keys
     * @param array $header
     *
     * @throws OpenIDConnectClientException
     */
    private function getKeyForHeader(array $keys, array $header): array
    {
        foreach ($keys as $key) {
            if ($key['kty'] === 'RSA') {
                if (!isset($header['kid']) || ($key['kid'] === $header['kid'])) {
                    return $key;
                }
            } elseif (isset($key['alg']) && ($key['alg'] === $header['alg']) && ($key['kid'] === $header['kid'])) {
                return $key;
            }
        }

        if ($this->additionalJwks) {
            foreach ($this->additionalJwks as $key) {
                if ($key['kty'] === 'RSA') {
                    if (!isset($header['kid']) || ($key['kid'] === $header['kid'])) {
                        return $key;
                    }
                } elseif (isset($key['alg']) && ($key['alg'] === $header['alg']) && ($key['kid'] === $header['kid'])) {
                    return $key;
                }
            }
        }

        if (isset($header['kid'])) {
            throw new OpenIDConnectClientException(
                'Unable to find a key for (algorithm, kid):' . $header['alg'] . ', ' . $header['kid'] . ')'
            );
        }

        throw new OpenIDConnectClientException('Unable to find a key for RSA');
    }


    /**
     * @param string $hashtype
     * @param array  $key
     * @param string $payload
     * @param string $signature
     * @param string $signatureType
     *
     * @return bool
     * @throws OpenIDConnectClientException
     */
    private function verifyRSAJWTSignature(
        string $hashtype,
        array  $key,
        string $payload,
        string $signature,
        string $signatureType
    ): bool {
        if (!array_key_exists('n', $key) || !array_key_exists('e', $key)) {
            throw new OpenIDConnectClientException('Malformed key object');
        }

        /* We already have base64url-encoded data, so re-encode it as
           regular base64 and use the XML key format for simplicity.
        */
        $public_key_xml = "<RSAKeyValue>\r\n" .
                          '  <Modulus>' . self::b64url2b64($key['n']) . "</Modulus>\r\n" .
                          '  <Exponent>' . self::b64url2b64($key['e']) . "</Exponent>\r\n" .
                          '</RSAKeyValue>';

        $rsa = new \phpseclib\Crypt\RSA();
        $rsa->setHash($hashtype);

        if ($signatureType === 'PSS') {
            $rsa->setMGFHash($hashtype);
        }

        $rsa->loadKey($public_key_xml, \phpseclib\Crypt\RSA::PUBLIC_FORMAT_XML);

        $rsa->setSignatureMode(
            ($signatureType === 'PSS') ? \phpseclib\Crypt\RSA::SIGNATURE_PSS : \phpseclib\Crypt\RSA::SIGNATURE_PKCS1
        );

        return $rsa->verify($payload, $signature);
    }

    /**
     * @param string $hashtype
     * @param string $key
     * @param string $payload
     * @param string $signature
     *
     * @return bool
     */
    private function verifyHMACJWTSignature(string $hashtype, string $key, string $payload, string $signature): bool
    {
        return hash_equals($signature, hash_hmac($hashtype, $payload, $key, true));
    }
}