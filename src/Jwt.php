<?php

namespace Jumbojett;

class Jwt
{
    use UrlEncoderTrait;

    private string $header;
    private string $payload;
    private string $signature;

    public function __construct(string $jwt)
    {
        $parts = explode('.', $jwt);

        if (count($parts) !== 3) {
            throw new \Exception();
        }

        $this->header    = $parts[0];
        $this->payload   = $parts[1];
        $this->signature = $parts[2];
    }

    public function __toString(): string
    {
        return "{$this->getHeader()}.{$this->getPayload()}.{$this->getSignature()}";
    }

    public function getHeader(): string
    {
        return $this->header;
    }

    public function getDecodedHeader(): array
    {
        return self::decode($this->getHeader());
    }

    public function getPayload(): string
    {
        return $this->payload;
    }

    public function getDecodedPayload(): array
    {
        return self::decode($this->getPayload());
    }

    public function getSignature(): string
    {
        return $this->signature;
    }

    public function getDecodedSignature(): string
    {
        return self::base64url_decode($this->getSignature());
    }

    public function getHeaderPayload(): string
    {
        return "{$this->getHeader()}.{$this->getPayload()}";
    }

    private static function decode(string $section): array
    {
        return json_decode(self::base64url_decode($section), true);
    }

    public function getHeaderAlg(): string
    {
        $header = $this->getDecodedHeader();

        if (!array_key_exists('alg', $header)) {
            throw new \Exception();
        }

        return $header['alg'];
    }

    public function getExpirationTimestamp(): int
    {
        $payload = $this->getDecodedPayload();

        return $payload['exp'] ?? throw new \Exception();
    }

    public function hasExpired(int $leeway = 0): bool
    {
        $exp = $this->getExpirationTimestamp();

        return (($exp !== null) && ($exp < (time() - $leeway)));
    }
}