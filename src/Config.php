<?php

namespace Jumbojett;

class Config
{
    public const AUTHORIZATION_ENDPOINT = 'authorization_endpoint';
    public const TOKEN_ENDPOINT         = 'token_endpoint';
    public const USER_INFO_ENDPOINT     = 'userinfo_endpoint';
    public const END_SESSION_ENDPOINT   = 'end_session_endpoint';
    public const REGISTRATION_ENDPOINT  = 'registration_endpoint';
    public const INTROSPECTION_ENDPOINT = 'introspection_endpoint';
    public const REVOCATION_ENDPOINT    = 'revocation_endpoint';

    public const JWKS_URI                              = 'jwks_uri';
    public const CODE_CHALLENGE_METHODS_SUPPORTED      = 'code_challenge_methods_supported';
    public const TOKEN_ENDPOINT_AUTH_METHODS_SUPPORTED = 'token_endpoint_auth_methods_supported';

    private string $provider_url;
    private string $client_id;
    private string $client_secret;

    private string $issuer;

    private array $well_known_values = [];

    private int $timeout         = 3;
    private int $connect_timeout = 3;

    private int $leeway = 300;

    private bool $state_and_nonce_in_redirect_uri = false;

    private ?string $well_known_config_url = null;

    /** @var ?callable */
    private $claims_validator;

    private array $auth_params = [];

    private array $scopes = [
        'openid',
    ];

    private array $response_types = [];

    private ?string $code_challenge_method = null;

    private int $enc_type = PHP_QUERY_RFC1738;

    private ?string $state_prefix = null;

    /**
     * @param string $provider_url
     * @param string $client_id
     * @param string $client_secret
     */
    public function __construct(string $provider_url, string $client_id, string $client_secret)
    {
        $this->provider_url  = rtrim($provider_url, '/');
        $this->client_id     = $client_id;
        $this->client_secret = $client_secret;

        $this->setIssuer($this->provider_url);
        $this->setWellKnownConfigUrl($this->provider_url . '/.well-known/openid-configuration');
    }

    public function getClientId(): string
    {
        return $this->client_id;
    }

    public function getClientSecret(): string
    {
        return $this->client_secret;
    }

    public function setIssuer(string $issuer): void
    {
        $this->issuer = $issuer;
    }

    public function getIssuer(): string
    {
        return $this->issuer;
    }

    public function setWellKnownConfigUrl(string $url): void
    {
        $this->well_known_config_url = $url;
    }

    public function getWellKnownConfigUrl(): ?string
    {
        return $this->well_known_config_url;
    }

    public function setTimeout(int $timeout): void
    {
        $this->timeout = $timeout;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function setConnectionTimeout(int $timeout): void
    {
        $this->connect_timeout = $timeout;
    }

    public function getConnectionTimeout(): int
    {
        return $this->connect_timeout;
    }

    public function setLeeway(int $leeway): void
    {
        $this->leeway = $leeway;
    }

    public function getLeeway(): int
    {
        return $this->leeway;
    }

    public function setAuthorizationEndpointUrl(string $url): void
    {
        $this->setWellKnownValue(self::AUTHORIZATION_ENDPOINT, $url);
    }

    public function setTokenEndpointUrl(string $url): void
    {
        $this->setWellKnownValue(self::TOKEN_ENDPOINT, $url);
    }

    public function setUserInfoEndpointUrl(string $url): void
    {
        $this->setWellKnownValue(self::USER_INFO_ENDPOINT, $url);
    }

    public function setLogoutEndpointUrl(string $url): void
    {
        $this->setWellKnownValue(self::END_SESSION_ENDPOINT, $url);
    }

    public function setRegistrationEndpointUrl(string $url): void
    {
        $this->setWellKnownValue(self::REGISTRATION_ENDPOINT, $url);
    }

    public function setIntrospectionEndpointUrl(string $url): void
    {
        $this->setWellKnownValue(self::INTROSPECTION_ENDPOINT, $url);
    }

    public function setRevocationEndpointUrl(string $url): void
    {
        $this->setWellKnownValue(self::REVOCATION_ENDPOINT, $url);
    }

    public function setJwksUri(string $url): void
    {
        $this->setWellKnownValue(self::JWKS_URI, $url);
    }

    // Todo: Needed?
    public function setCodeChallengeMethodsSupported(array $methods): void
    {
        $this->setWellKnownValue(self::CODE_CHALLENGE_METHODS_SUPPORTED, $methods);
    }

    public function setTokenEndpointAuthMethodsSupported(array $methods): void
    {
        $this->setWellKnownValue(self::TOKEN_ENDPOINT_AUTH_METHODS_SUPPORTED, $methods);
    }

    public function setStateAndNonceInRedirectUri(bool $enable): void
    {
        $this->state_and_nonce_in_redirect_uri = $enable;
    }

    public function includeStateAndNonceInRedirectUri(): bool
    {
        return $this->state_and_nonce_in_redirect_uri;
    }

    public function addClaimsValidator(callable $validator): void
    {
        $this->claims_validator = $validator;
    }

    public function getClaimsValidator(): ?callable
    {
        return $this->claims_validator;
    }

    public function addAuthParams(array $params): void
    {
        $this->auth_params = array_merge($this->auth_params, $params);
    }

    public function getAuthParams(): array
    {
        return $this->auth_params;
    }

    public function addScopes(array $scopes): void
    {
        $this->scopes = array_unique(array_merge($this->scopes, $scopes));
    }

    public function getScopes(): array
    {
        return $this->scopes;
    }

    public function addResponseTypes(array $response_types): void
    {
        $this->response_types = array_unique(array_merge($this->response_types, $response_types));
    }

    public function getResponseTypes(): array
    {
        return $this->response_types;
    }

    public function setCodeChallengeMethod(string $code_challenge_method): void
    {
        $this->code_challenge_method = $code_challenge_method;
    }

    public function getCodeChallengeMethod(): ?string
    {
        return $this->code_challenge_method;
    }

    public function setUrlEncoding(int $encoding): void
    {
        switch ($encoding) {
            case PHP_QUERY_RFC1738:
                $this->enc_type = PHP_QUERY_RFC1738;
                break;

            case PHP_QUERY_RFC3986:
                $this->enc_type = PHP_QUERY_RFC3986;
                break;

            default:
                break;
        }
    }

    public function getUrlEncoding(): int
    {
        return $this->enc_type;
    }

    /**
     * @param string $param
     * @param mixed  $default
     *
     * @return mixed
     */
    public function getWellKnownValue(string $param, $default = null)
    {
        return $this->well_known_values[$param] ?? $default;
    }

    /**
     * @param string $param
     * @param mixed  $value
     *
     * @return void
     */
    public function setWellKnownValue(string $param, $value): void
    {
        $this->well_known_values[$param] = $value;
    }

    public function setWellKnownValues(array $well_known): void
    {
        $this->well_known_values = array_merge($this->well_known_values, $well_known);
    }

    public function setStatePrefix(string $prefix): void
    {
        $this->state_prefix = $prefix;
    }

    public function getStatePrefix(): ?string
    {
        return $this->state_prefix;
    }
}
