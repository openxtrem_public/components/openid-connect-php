<?php

namespace Jumbojett;

use Error;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use function bin2hex;
use function random_bytes;

class Client
{
    private const SESSION_NONCE_KEY         = 'openid_connect_nonce';
    private const SESSION_STATE_KEY         = 'openid_connect_state';
    private const SESSION_CODE_VERIFIER_KEY = 'openid_connect_code_verifier';

    private HttpClientInterface $client;

    private Config $config;

    private JwtValidator $validator;

    private ?string $id_token = null;

    private ?string $access_token = null;

    private ?string $refresh_token = null;

    private array $verified_claims;

    private ?int $response_code = null;

    public function __construct(HttpClientInterface $client, Config $config)
    {
        $this->client    = $client;
        $this->config    = $config;
        $this->validator = new JwtValidator();
    }

    public function requestAuthorization(string $redirect_url): string
    {
        $auth_endpoint = $this->getAuthorizationEndpointUrl();

        // Generate and store a nonce in the session
        // The nonce is an arbitrary value
        $nonce = $this->setNonce($this->generateRandString());

        // State essentially acts as a session key for OIDC
        $state = $this->setState($this->generateState());

        $auth_params = array_merge($this->config->getAuthParams(), [
            'response_type' => 'code',
            'redirect_uri'  => $redirect_url,
            'client_id'     => $this->config->getClientId(),
            'nonce'         => $nonce,
            'state'         => $state,
            'scope'         => implode(' ', $this->config->getScopes()),
        ]);

        // If identity provider needs params explicitly set in the redirect callback
        if ($this->config->includeStateAndNonceInRedirectUri()) {
            $auth_params['redirect_uri'] .=
                (strpos($auth_params['redirect_uri'], '?') === false)
                    ? "?state={$state}&nonce={$nonce}"
                    : "&state={$state}&nonce={$nonce}";
        }

        // If the client has been registered with additional response types
        if (count($this->config->getResponseTypes()) > 0) {
            $auth_params = array_merge(
                $auth_params,
                ['response_type' => implode(' ', $this->config->getResponseTypes())]
            );
        }

        // If the client supports Proof Key for Code Exchange (PKCE)
        $ccm = $this->config->getCodeChallengeMethod();

        if (
            ($ccm !== null)
            && in_array($ccm, $this->getCodeChallengeMethodsSupported())
        ) {
            $codeVerifier = bin2hex(random_bytes(64));

            $this->setCodeVerifier($codeVerifier);

            if (!empty($this->pkceAlgs[$ccm])) {
                $codeChallenge = rtrim(
                    strtr(
                        base64_encode(hash($this->pkceAlgs[$ccm], $codeVerifier, true)),
                        '+/',
                        '-_'
                    ),
                    '='
                );
            } else {
                $codeChallenge = $codeVerifier;
            }

            $auth_params = array_merge($auth_params, [
                'code_challenge'        => $codeChallenge,
                'code_challenge_method' => $ccm,
            ]);
        }

        $auth_endpoint .=
            ((strpos($auth_endpoint, '?') === false) ? '?' : '&')
            . http_build_query($auth_params, null, '&', $this->config->getUrlEncoding());

        $this->commitSession();

        return $auth_endpoint;
    }

    //    public function implicitFlow(string $state, Jwt $id_token, ?string $access_token)
    //    {
    //        // Do an OpenID Connect session check
    //        if ($state !== $this->getState()) {
    //            throw new OpenIDConnectClientException('Unable to determine state');
    //        }
    //
    //        // Cleanup state
    //        $this->unsetState();
    //
    //        $claims = $id_token->getDecodedPayload();
    //
    //        // Verify the signature
    //
    //        // Since jwks_uri config is only present when OPTIONAL .well-known is set, we check at the last moment.
    //        $jwks_accessor = fn() => $this->fetchURL($this->getJwksUri());
    //
    //        if (!$this->validator->verifyJwtSignature($id_token, $jwks_accessor, $this->config->getClientSecret())) {
    //            throw new OpenIDConnectClientException('Unable to verify signature');
    //        }
    //
    //        // Save the id token
    //        $this->id_token = (string)$id_token;
    //
    //        // If this is a valid claim
    //        if ($this->validator->verifyJWTclaims($claims, $id_token, $access_token, [$this, 'claimsValidator'])) {
    //            // Clean up the session a little
    //            $this->unsetNonce();
    //
    //            // Save the verified claims
    //            $this->verified_claims = $claims;
    //
    //            // Save the access token
    //            if ($access_token) {
    //                $this->access_token = $access_token;
    //            }
    //
    //            // Success!
    //            return true;
    //        }
    //
    //        throw new OpenIDConnectClientException('Unable to verify JWT claims');
    //    }

    public function signOut(string $idToken, ?string $redirect): string
    {
        $signout_endpoint = $this->getEndSessionEndpointUrl();

        $signout_params = ['id_token_hint' => $idToken];

        if ($redirect !== null) {
            $signout_params['post_logout_redirect_uri'] = $redirect;
        }

        $signout_endpoint .=
            ((strpos($signout_endpoint, '?') === false) ? '?' : '&')
            . http_build_query(
                $signout_params,
                null,
                '&',
                $this->config->getUrlEncoding()
            );

        return $signout_endpoint;
    }

    //    /**
    //     * Requests a client credentials token
    //     *
    //     * @throws OpenIDConnectClientException
    //     */
    //    public function requestClientCredentialsToken()
    //    {
    //        $token_endpoint = $this->getTokenEndpointUrl();
    //
    //        $post_data = [
    //            'grant_type'    => 'client_credentials',
    //            'client_id'     => $this->config->getClientId(),
    //            'client_secret' => $this->config->getClientSecret(),
    //            'scope'         => implode(' ', $this->config->getScopes()),
    //        ];
    //
    //        return $this->fetchURL($token_endpoint, $post_data, [], true);
    //    }
    //
    //    /**
    //     * Requests a resource owner token
    //     * (Defined in https://tools.ietf.org/html/rfc6749#section-4.3)
    //     *
    //     * @param bool $use_client_auth Indicates that the Client ID and Secret be used for client authentication
    //     *
    //     * @return mixed
    //     * @throws OpenIDConnectClientException
    //     */
    //    public function requestResourceOwnerToken(bool $use_client_auth, string $username, string $password)
    //    {
    //        $token_endpoint = $this->getTokenEndpointUrl();
    //
    //        $headers = [];
    //
    //        $post_data = [
    //            'grant_type' => 'password',
    //            'username'   => $username,
    //            'password'   => $password,
    //            'scope'      => implode(' ', $this->config->getScopes()),
    //        ];
    //
    //        //For client authentication include the client values
    //        if ($use_client_auth) {
    //            $token_endpoint_auth_methods_supported = $this->getTokenEndpointAuthMethodsSupported();
    //
    //            if (in_array('client_secret_basic', $token_endpoint_auth_methods_supported, true)) {
    //                $headers = [
    //                    'Authorization' => 'Basic '
    //                        . base64_encode(
    //                            urlencode($this->config->getClientId())
    //                            . ':'
    //                            . urlencode($this->config->getClientSecret())
    //                        ),
    //                ];
    //            } else {
    //                $post_data['client_id']     = $this->config->getClientId();
    //                $post_data['client_secret'] = $this->config->getClientSecret();
    //            }
    //        }
    //
    //        return $this->fetchURL($token_endpoint, $post_data, $headers, true);
    //    }

    /**
     * Stores nonce
     *
     * @param string $nonce
     *
     * @return string
     */
    private function setNonce(string $nonce): string
    {
        $this->setSessionKey(self::SESSION_NONCE_KEY, $nonce);

        return $nonce;
    }

    /**
     * Get stored nonce
     *
     * @return string
     */
    private function getNonce(): ?string
    {
        return $this->getSessionKey(self::SESSION_NONCE_KEY);
    }

    /**
     * Cleanup nonce
     *
     * @return void
     */
    private function unsetNonce(): void
    {
        $this->unsetSessionKey(self::SESSION_NONCE_KEY);
    }

    /**
     * Stores $state
     *
     * @param string $state
     *
     * @return string
     */
    private function setState(string $state): string
    {
        $this->setSessionKey(self::SESSION_STATE_KEY, $state);

        return $state;
    }

    /**
     * Get stored state
     *
     * @return string
     */
    private function getState(): ?string
    {
        return $this->getSessionKey(self::SESSION_STATE_KEY);
    }

    /**
     * Cleanup state
     *
     * @return void
     */
    private function unsetState(): void
    {
        $this->unsetSessionKey(self::SESSION_STATE_KEY);
    }

    /**
     * Stores $codeVerifier
     *
     * @param string $codeVerifier
     *
     * @return string
     */
    private function setCodeVerifier(string $codeVerifier): string
    {
        $this->setSessionKey(self::SESSION_CODE_VERIFIER_KEY, $codeVerifier);

        return $codeVerifier;
    }

    /**
     * Get stored codeVerifier
     *
     * @return string
     */
    private function getCodeVerifier(): ?string
    {
        return $this->getSessionKey(self::SESSION_CODE_VERIFIER_KEY);
    }

    /**
     * Cleanup codeVerifier
     *
     * @return void
     */
    private function unsetCodeVerifier(): void
    {
        $this->unsetSessionKey(self::SESSION_CODE_VERIFIER_KEY);
    }

    /**
     * Used for arbitrary value generation for nonce and state
     *
     * @return string
     * @throws OpenIDConnectClientException
     */
    private function generateRandString()
    {
        // Error and Exception need to be caught in this order, see https://github.com/paragonie/random_compat/blob/master/README.md
        // random_compat polyfill library should be removed if support for PHP versions < 7 is dropped
        try {
            return bin2hex(random_bytes(16));
        } catch (Error|Exception $e) {
            throw new OpenIDConnectClientException('Random token generation failed.');
        }
    }

    private function generateState(): string
    {
        $state  = $this->generateRandString();
        $prefix = $this->config->getStatePrefix();

        if ($prefix !== null) {
            $state = "{$prefix}-{$state}";
        }

        return $state;
    }

    /**
     * Use session to manage a nonce
     */
    protected function startSession()
    {
        if (!isset($_SESSION)) {
            @session_start();
        }
    }

    protected function commitSession()
    {
        $this->startSession();

        session_write_close();
    }

    protected function getSessionKey($key)
    {
        $this->startSession();

        if (array_key_exists($key, $_SESSION)) {
            return $_SESSION[$key];
        }

        return false;
    }

    protected function setSessionKey($key, $value)
    {
        $this->startSession();

        $_SESSION[$key] = $value;
    }

    protected function unsetSessionKey($key)
    {
        $this->startSession();

        unset($_SESSION[$key]);
    }

    public function requestTokens(string $redirect_url, string $code, string $state, ?string $nonce): bool
    {
        $token_endpoint = $this->getTokenEndpointUrl();

        $token_endpoint_auth_methods_supported = $this->getTokenEndpointAuthMethodsSupported();

        $headers = [];

        $token_params = [
            'grant_type'    => 'authorization_code',
            'code'          => $code,
            'redirect_uri'  => $redirect_url,
            'client_id'     => $this->config->getClientId(),
            'client_secret' => $this->config->getClientSecret(),
        ];

        // If identity provider needs params explicitly set in the redirect callback
        if ($this->config->includeStateAndNonceInRedirectUri()) {
            $token_params['redirect_uri'] .= "?state={$state}&nonce={$nonce}";
        }

        # Consider Basic authentication if provider config is set this way
        if (in_array('client_secret_basic', $token_endpoint_auth_methods_supported, true)) {
            $headers = [
                'Authorization' => 'Basic '
                                   . base64_encode(
                                       urlencode($this->config->getClientId())
                                       . ':'
                                       . urlencode($this->config->getClientSecret())
                                   ),
            ];

            unset($token_params['client_secret']);
            unset($token_params['client_id']);
        }

        $ccm = $this->config->getCodeChallengeMethod();
        $cv  = $this->getCodeVerifier();

        if (($ccm !== null) && ($cv !== null)) {
            $headers = [];
            unset($token_params['client_secret']);

            $token_params = array_merge($token_params, [
                'client_id'     => $this->config->getClientId(),
                'code_verifier' => $cv,
            ]);
        }

        $token = $this->fetchURL($token_endpoint, $token_params, $headers, true);

        return $this->checkTokenResponse($token, $state);
    }

    /**
     * @throws OpenIDConnectClientException
     */
    private function checkTokenResponse(array $token_response, string $state)
    {
        if (array_key_exists('error', $token_response)) {
            if (array_key_exists('error_description', $token_response)) {
                throw new OpenIDConnectClientException($token_response['error_description']);
            }

            throw new OpenIDConnectClientException('Got response: ' . $token_response['error']);
        }

        // Do an OpenID Connect session check
        if ($state !== $this->getState()) {
            throw new OpenIDConnectClientException('Unable to determine state');
        }

        // Cleanup state
        $this->unsetState();

        if (!array_key_exists('id_token', $token_response)) {
            throw new OpenIDConnectClientException('User did not authorize openid scope.');
        }

        $id_token = new Jwt($token_response['id_token']);

        $claims = $id_token->getDecodedPayload();

        // Since jwks_uri config is only present when OPTIONAL .well-known is set, we check at the last moment.
        $jwks_accessor = fn() => $this->fetchURL($this->getJwksUri());

        // Verify the signature
        if (!$this->validator->verifyJwtSignature($id_token, $jwks_accessor, $this->config->getClientSecret())) {
            throw new OpenIDConnectClientException('Unable to verify signature');
        }

        // Save the id token
        $this->id_token = (string)$id_token;

        // Save the access token
        $this->access_token = $token_response['access_token'];

        // If this is a valid claim
        if (
            $this->validator->verifyJwtClaims(
                $claims,
                $id_token,
                $token_response['access_token'],
                [$this, 'claimsValidator']
            )
        ) {
            // Clean up the session a little
            $this->unsetNonce();

            // Save the verified claims
            $this->verified_claims = $claims;

            // Save the refresh token, if we got one
            if (isset($token_response['refresh_token'])) {
                $this->refresh_token = $token_response['refresh_token'];
            }

            // Success!
            return true;
        }

        throw new OpenIDConnectClientException('Unable to verify JWT claims');
    }

    /**
     * Requests Access token with refresh token
     *
     * @param string $refresh_token
     *
     * @return array
     * @throws OpenIDConnectClientException
     */
    public function refreshToken(string $refresh_token): array
    {
        $token_endpoint                        = $this->getTokenEndpointUrl();
        $token_endpoint_auth_methods_supported = $this->getTokenEndpointAuthMethodsSupported();

        $headers = [];

        $token_params = [
            'grant_type'    => 'refresh_token',
            'refresh_token' => $refresh_token,
            'client_id'     => $this->config->getClientId(),
            'client_secret' => $this->config->getClientSecret(),
            'scope'         => implode(' ', $this->config->getScopes()),
        ];

        // Consider Basic authentication if provider config is set this way
        if (in_array('client_secret_basic', $token_endpoint_auth_methods_supported, true)) {
            $headers['Authorization'] = 'Basic '
                                        . base64_encode(
                                            urlencode($this->config->getClientId())
                                            . ':'
                                            . urlencode($this->config->getClientSecret())
                                        );

            unset($token_params['client_secret']);
            unset($token_params['client_id']);
        }

        $json = $this->fetchURL($token_endpoint, $token_params, $headers, true);

        if (isset($json['access_token'])) {
            $this->access_token = $json['access_token'];
        }

        if (isset($json['refresh_token'])) {
            $this->refresh_token = $json['refresh_token'];
        }

        return $json;
    }

    //    /**
    //     * Dynamic registration
    //     *
    //     * @throws OpenIDConnectClientException
    //     */
    //    public function register(string $redirect_url)
    //    {
    //        $registration_endpoint = $this->getRegistrationEndpointUrl();
    //
    //        $send = array_merge($this->registrationParams, [
    //            'redirect_uris' => [$redirect_url],
    //            'client_name'   => $this->getClientName(),
    //        ]);
    //
    //        $json_response = $this->fetchURL($registration_endpoint, $send);
    //
    //        // Throw some errors if we encounter them
    //        if ($json_response === false) {
    //            throw new OpenIDConnectClientException(
    //                'Error registering: JSON response received from the server was invalid.'
    //            );
    //        }
    //
    //        if (isset($json_response['error_description'])) {
    //            throw new OpenIDConnectClientException($json_response['error_description']);
    //        }
    //
    //        $this->config->setClientId($json_response['client_id']);
    //
    //        // The OpenID Connect Dynamic registration protocol makes the client secret optional
    //        // and provides a registration access token and URI endpoint if it is not present
    //        if (isset($json_response['client_secret'])) {
    //            $this->config->setClientSecret($json_response['client_secret']);
    //        } else {
    //            throw new OpenIDConnectClientException(
    //                'Error registering:
    //                                                    Please contact the OpenID Connect provider and obtain a Client ID and Secret directly from them'
    //            );
    //        }
    //    }
    //
    //    /**
    //     * Introspect a given token - either access token or refresh token.
    //     * @see https://tools.ietf.org/html/rfc7662
    //     *
    //     * @param string      $token
    //     * @param string      $token_type_hint
    //     * @param string|null $clientId
    //     * @param string|null $clientSecret
    //     *
    //     * @return mixed
    //     * @throws OpenIDConnectClientException
    //     */
    //    public function introspectToken(
    //        string $token,
    //        string $token_type_hint = null,
    //        string $client_id = null,
    //        string $client_secret = null
    //    ) {
    //        $introspection_endpoint = $this->getIntrospectionEndpointUrl();
    //
    //        $post_data = [
    //            'token' => $token,
    //        ];
    //
    //        if ($token_type_hint !== null) {
    //            $post_data['token_type_hint'] = $token_type_hint;
    //        }
    //
    //        $client_id     = ($client_id) ?: $this->config->getClientId();
    //        $client_secret = ($client_secret) ?: $this->config->getClientSecret();
    //
    //        $headers = [
    //            'Authorization' => 'Basic ' . base64_encode(urlencode($client_id) . ':' . urlencode($client_secret)),
    //            'Accept'        => 'application/json',
    //        ];
    //
    //        return $this->fetchURL($introspection_endpoint, $post_data, $headers);
    //    }

    /**
     *
     * @param string $access_token
     *
     * Attribute        Type        Description
     * user_id          string      REQUIRED Identifier for the End-User at the Issuer.
     * name             string      End-User's full name in displayable form including all name parts, ordered
     * according to End-User's locale and preferences. given_name       string      Given name or first name of the
     * End-User. family_name      string      Surname or last name of the End-User. middle_name      string      Middle
     * name of the End-User. nickname         string      Casual name of the End-User that may or may not be the same
     * as the given_name. For instance, a nickname value of Mike might be returned alongside a given_name value of
     * Michael. profile          string      URL of End-User's profile page. picture          string      URL of the
     * End-User's profile picture. website          string      URL of End-User's web page or blog. email
     * string      The End-User's preferred e-mail address. verified         boolean     True if the End-User's e-mail
     * address has been verified; otherwise false. gender           string      The End-User's gender: Values defined
     * by this specification are female and male. Other values MAY be used when neither of the defined values are
     * applicable. birthday         string      The End-User's birthday, represented as a date string in MM/DD/YYYY
     * format. The year MAY be 0000, indicating that it is omitted. zoneinfo         string      String from zoneinfo
     * [zoneinfo] time zone database. For example, Europe/Paris or America/Los_Angeles. locale           string
     * The End-User's locale, represented as a BCP47 [RFC5646] language tag. This is typically an ISO 639-1 Alpha-2
     * [ISO639‑1] language code in lowercase and an ISO 3166-1 Alpha-2 [ISO3166‑1] country code in uppercase, separated
     * by a dash. For example, en-US or fr-CA. As a compatibility note, some implementations have used an underscore as
     * the separator rather than a dash, for example, en_US; Implementations MAY choose to accept this locale syntax as
     * well. phone_number     string      The End-User's preferred telephone number. E.164 [E.164] is RECOMMENDED as
     * the format of this Claim. For example, +1 (425) 555-1212 or +56 (2) 687 2400. address          JSON object The
     * End-User's preferred address. The value of the address member is a JSON [RFC4627] structure containing some or
     * all of the members defined in Section 2.4.2.1. updated_time     string      Time the End-User's information was
     * last updated, represented as a RFC 3339 [RFC3339] datetime. For example, 2011-01-03T23:58:42+0000.
     *
     * @return mixed
     *
     * @throws OpenIDConnectClientException
     */
    public function requestUserInfo(string $access_token)
    {
        $user_info_endpoint = $this->getUserInfoEndpointUrl();
        $schema             = 'openid';

        $user_info_endpoint .= '?schema=' . $schema;

        //The accessToken has to be sent in the Authorization header.
        // Accept json to indicate response type
        $headers = [
            'Authorization' => "Bearer {$access_token}",
            'Accept'        => 'application/json',
        ];

        $user_info = $this->fetchURL($user_info_endpoint, [], $headers);

        if ($this->getResponseCode() !== 200) {
            throw new OpenIDConnectClientException(
                'The communication to retrieve user data has failed with status code ' . $this->getResponseCode()
            );
        }

        return $user_info;
    }

    public function getIdToken(): ?string
    {
        return $this->id_token;
    }

    public function getAccessToken(): ?string
    {
        return $this->access_token;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refresh_token;
    }

    /**
     * Attribute        Type    Description
     * exp              int     Expires at
     * nbf              int     Not before
     * ver              string  Version
     * iss              string  Issuer
     * sub              string  Subject
     * aud              string  Audience
     * nonce            string  nonce
     * iat              int     Issued At
     * auth_time        int     Authentication time
     * oid              string  Object id
     *
     */
    public function getVerifiedClaims(): array
    {
        return $this->verified_claims;
    }

    private function getResponseCode(): ?int
    {
        return $this->response_code;
    }

    /**
     * Revoke a given token - either access token or refresh token.
     * @see https://tools.ietf.org/html/rfc7009
     *
     * @param string      $token
     * @param string      $token_type_hint
     * @param string|null $client_id
     * @param string|null $client_secret
     *
     * @return mixed
     * @throws OpenIDConnectClientException
     */
    public function revokeToken(
        string $token,
        string $token_type_hint = null,
        string $client_id = null,
        string $client_secret = null
    ) {
        $revocation_endpoint = $this->getRevocationEndpointUrl();

        $post_data = [
            'token' => $token,
        ];

        if ($token_type_hint !== null) {
            $post_data['token_type_hint'] = $token_type_hint;
        }

        $client_id     = ($client_id) ?: $this->config->getClientId();
        $client_secret = ($client_secret) ?: $this->config->getClientSecret();

        $headers = [
            'Authorization' => 'Basic ' . base64_encode(urlencode($client_id) . ':' . urlencode($client_secret)),
            'Accept'        => 'application/json',
        ];

        return $this->fetchURL($revocation_endpoint, $post_data, $headers, true);
    }

    /**
     * @param string $url
     * @param array  $post_body If this is set the post type will be POST
     * @param array  $headers   Extra headers to be send with the request. Format as 'NameHeader: ValueHeader'
     * @param bool   $form_params
     *
     * @return array
     * @throws OpenIDConnectClientException
     */
    protected function fetchURL(
        string $url,
        array  $post_body = [],
        array  $headers = [],
        bool   $form_params = false
    ): array {
        $method = 'GET';

        $options = [
            'max_redirects' => 5,
            //            'timeout'       => $this->config->getConnectionTimeout(),
            'max_duration'  => $this->config->getTimeout(),
        ];

        // Determine whether this is a GET or POST
        if (count($post_body) > 0) {
            $method = 'POST';

            if ($form_params) {
                $options['body']         = $post_body;
                $headers['Content-Type'] = 'application/x-www-form-urlencoded';
            } else {
                $options['json']         = $post_body;
                $headers['Content-Type'] = 'application/json';
            }
        }

        if (count($headers) > 0) {
            $options['headers'] = $headers;
        }

        try {
            $response            = $this->client->request($method, $url, $options);
            $this->response_code = $response->getStatusCode();
            $json                = json_decode($response->getContent(), true);
        } catch (ClientExceptionInterface|RedirectionExceptionInterface|ServerExceptionInterface|TransportExceptionInterface $e) {
            throw new OpenIDConnectClientException($e->getMessage());
        }

        if (!is_array($json)) {
            throw new OpenIDConnectClientException();
        }

        return $json;
    }

    public function claimsValidator(array $claims, ?string $access_token, ?string $expected_at_hash): bool
    {
        $validated = $this->issValidator($claims['iss'])
                     && $this->audValidator($claims['aud'])
                     && $this->nonceValidator($claims['nonce'] ?? null)
                     && $this->expValidator($claims['exp'] ?? null)
                     && $this->nbfValidator($claims['nbf'] ?? null)
                     && $this->atHashValidator($claims['at_hash'] ?? null, $access_token, $expected_at_hash);

        $custom_validator = $this->config->getClaimsValidator();

        if (is_callable($custom_validator)) {
            $validated = $validated && call_user_func($custom_validator, $this, $claims);
        }

        return $validated;
    }

    private function issValidator(string $iss): bool
    {
        return ($iss === $this->config->getIssuer())
               || ($iss === $this->getWellKnownIssuer())
               || ($iss === $this->getWellKnownIssuer(true));
    }

    /**
     * @param string|array $aud
     *
     * @return bool
     */
    private function audValidator($aud): bool
    {
        return ($aud === $this->config->getClientId()) || in_array($this->config->getClientId(), $aud, true);
    }

    private function nonceValidator(?string $nonce): bool
    {
        return is_null($nonce) || ($nonce === $this->getNonce());
    }

    private function expValidator(?int $exp): bool
    {
        return is_null($exp) || ($exp >= (time() - $this->config->getLeeway()));
    }

    private function nbfValidator(?int $nbf): bool
    {
        return is_null($nbf) || ($nbf <= (time() + $this->config->getLeeway()));
    }

    private function atHashValidator(?string $at_hash, ?string $access_token, ?string $expected_at_hash): bool
    {
        return is_null($at_hash) || is_null($access_token) || ($at_hash === $expected_at_hash);
    }

    /**
     * @param string $param
     *
     * @return mixed
     * @throws Exception
     */
    private function getProviderConfigValue(string $param)
    {
        $value = $this->config->getWellKnownValue($param);

        if ($value !== null) {
            return $value;
        }

        return $this->getWellKnownConfigValue($param);
    }

    /**
     * @param string $param
     *
     * @return mixed
     * @throws OpenIDConnectClientException
     */
    private function getWellKnownConfigValue(string $param)
    {
        $well_known_endpoint = $this->config->getWellKnownConfigUrl();

        if ($well_known_endpoint === null) {
            throw new Exception();
        }

        $well_known = $this->fetchURL($well_known_endpoint);
        $this->config->setWellKnownValues($well_known);

        return $this->config->getWellKnownValue($param);
    }

    private function getAuthorizationEndpointUrl(): string
    {
        return $this->getProviderConfigValue(Config::AUTHORIZATION_ENDPOINT);
    }

    private function getTokenEndpointUrl(): string
    {
        return $this->getProviderConfigValue(Config::TOKEN_ENDPOINT);
    }

    private function getUserInfoEndpointUrl(): string
    {
        return $this->getProviderConfigValue(Config::USER_INFO_ENDPOINT);
    }

    private function getEndSessionEndpointUrl(): string
    {
        return $this->getProviderConfigValue(Config::END_SESSION_ENDPOINT);
    }

    private function getRegistrationEndpointUrl(): string
    {
        return $this->getProviderConfigValue(Config::REGISTRATION_ENDPOINT);
    }

    private function getIntrospectionEndpointUrl(): string
    {
        return $this->getProviderConfigValue(Config::INTROSPECTION_ENDPOINT);
    }

    private function getRevocationEndpointUrl(): string
    {
        return $this->getProviderConfigValue(Config::REVOCATION_ENDPOINT);
    }

    private function getJwksUri(): string
    {
        return $this->getProviderConfigValue(Config::JWKS_URI);
    }

    private function getCodeChallengeMethodsSupported(): ?array
    {
        return $this->getProviderConfigValue(Config::CODE_CHALLENGE_METHODS_SUPPORTED);
    }

    private function getTokenEndpointAuthMethodsSupported(): array
    {
        return $this->getProviderConfigValue(Config::TOKEN_ENDPOINT_AUTH_METHODS_SUPPORTED);
    }
}